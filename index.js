// Repetition Control Structures

// While loop
/* Syntax:
	while(condition){
		statement/s;
	}
*/

// let count = 5;

// // count = 5
// while (count !==0){
// 	console.log("While: " + count);
// 	count--;
// }

// console.log("Displays numbers 1-10")
// count = 1;

// while(count <= 10){
// 	console.log("while: " + count);
// 	count++;
// }

// Do while loop

/* Syntax:
	do{
		statement;
	} while (condition);
*/


// Number() is similar to parseInt when converting String into numbers
let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);
	number +=1;
}while (number < 10);

// Create a new variable to be used in displaying even numbers from 2-10 using while loop

let evenNumber = 2

do{
	console.log("Even No.: " + evenNumber);
	evenNumber +=2;
}while (evenNumber < 11);

// For Loop
/* Syntax:
	for(initialization; condition; stepExpression){
		statement;
	}
*/

console.log("For Loop");

for(let count = 0; count <= 20; count++){
	console.log (count);
}

console.log ("Even For Loop");
let even = 2

for (let counter = 1; counter <=5; counter++ ){
	console.log("Even: " + even);
	even += 2;
}
/*
	counter = 1, 2, 3, 4, 5, 6
	even = 2, 4, 6, 8, 10, 12

*/
/* Expected output:
	Even: 2
	Even: 4
	Even: 6
	Even: 8
	Even: 10
*/

let myString = 'alex';
// .length property is used to count the characters in a string
console.log (myString.length);

console.log (myString[0]);
console.log(myString[3]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

/* Expected output:
	a
	l
	e
	x
*/

/* For reverse output:
	for(let x = 3; x>=0; x--;){
		console.log(myString[x]);
	}
OR
	for(let y = myString.length; y > 0; y--) {
		console.log(myString[y - 1]);
	}
*/

// Print out letter individually but will print 3 instead of the vowels
let myName = "AlEx";

for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
			console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}


// Continue and Break Statements

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0, tells the code to continue to iterate 
	if(count % 2 === 0){
		continue;
	}
	console.log("Continue and Break: " + count);

	// if the current value of count is greater than 10, tells the code to stop the loop
	if (count > 10){
		break;
	}
}

let name = 'alexandro'
/*
	name = 9
	i= 0,1,2,3,4,5,6
*/

for(let i = 0; i <name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i] === "d"){
		break;
	}
}










